package com.example.calculator;

import androidx.annotation.Dimension;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.udojava.evalex.Expression;
import com.udojava.evalex.ExpressionSettings;

import java.math.BigDecimal;
import java.math.MathContext;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    Button buttonOne;
    Button buttonTwo;
    Button buttonThree;
    Button buttonFour;
    Button buttonFive;
    Button buttonSix;
    Button buttonSeven;
    Button buttonEight;
    Button buttonNine;
    Button buttonZero;
    Button buttonEqual;
    Button buttonPercent;
    Button buttonClear;
    Button buttonPlus;
    Button buttonMultiply;
    Button buttonDivide;
    Button buttonDot;
    Button buttonMinus;
    Button buttonPlusMinus;

    // extended functions
    Button buttonLeftParenthesis;
    Button buttonRightParenthesis;
    Button buttonTenInPowerX;
    Button buttonLogTen;
    Button buttonPowerTwo;
    Button buttonPowerThree;
    Button buttonPowerY;
    Button buttonPowExpX;
    Button buttonSecondRoot;
    Button buttonThirdRoot;
    Button buttonFact;
    Button buttonLn;
    Button buttonSin;
    Button buttonCos;
    Button buttonTan;
    Button buttonExp;
    Button buttonASin;
    Button buttonACos;
    Button buttonATan;
    Button buttonPI;

    String fullExpression = "";

    String lastOperation = "";

    ExpressionSettings expressionSettings;

    // в моей реализации, после нажатия на кнопку операции поле ощищается.
    // при нажатии на новую кнопку операции, нужно проверять, на нулевое значение
    // Когда функция использует 2 значения, тогда нужно выделять кнопку функции и не очищать значение, а очищать значение, когда
    // вводится новая кнопка

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.TextView);

        buttonOne       = (Button) findViewById(R.id.ButtonOne);
        buttonTwo       = (Button) findViewById(R.id.ButtonTwo);
        buttonThree     = (Button) findViewById(R.id.ButtonThree);
        buttonFour      = (Button) findViewById(R.id.ButtonFour);
        buttonFive      = (Button) findViewById(R.id.ButtonFive);
        buttonSix       = (Button) findViewById(R.id.ButtonSix);
        buttonSeven     = (Button) findViewById(R.id.ButtonSeven);
        buttonEight     = (Button) findViewById(R.id.ButtonEight);
        buttonNine      = (Button) findViewById(R.id.ButtonNine);
        buttonZero      = (Button) findViewById(R.id.ButtonZero);
        buttonEqual     = (Button) findViewById(R.id.ButtonEqual);
        buttonPercent   = (Button) findViewById(R.id.ButtonPercent);
        buttonDivide    = (Button) findViewById(R.id.ButtonDiv);
        buttonMultiply  = (Button) findViewById(R.id.ButtonMul);
        buttonPlus      = (Button) findViewById(R.id.ButtonPlus);
        buttonMinus     = (Button) findViewById(R.id.ButtonMinus);
        buttonClear     = (Button) findViewById(R.id.ButtonClear);
        buttonDot       = (Button) findViewById(R.id.ButtonDot);
        buttonPlusMinus = (Button) findViewById(R.id.ButtonPlusMinus);


        buttonRightParenthesis = (Button) findViewById(R.id.ButtonRightParenthesis);
        buttonLeftParenthesis  = (Button) findViewById(R.id.ButtonLeftParenthesis);
        buttonTenInPowerX      = (Button) findViewById(R.id.ButtonTenInPowerX);
        buttonLogTen           = (Button) findViewById(R.id.ButtonLog10) ;
        buttonPowerTwo         = (Button) findViewById(R.id.ButtonPowTwo);
        buttonPowerThree       = (Button) findViewById(R.id.ButtonPowThree);
        buttonPowerY           = (Button) findViewById(R.id.ButtonPowY);
        buttonPowExpX          = (Button) findViewById(R.id.ButtonPowExpX);
        buttonSecondRoot       = (Button) findViewById(R.id.ButtonSecondRoot);
        buttonThirdRoot        = (Button) findViewById(R.id.ButtonThirdRoot);
        buttonFact             = (Button) findViewById(R.id.ButtonFact);
        buttonLn               = (Button) findViewById(R.id.ButtonLn);
        buttonSin              = (Button) findViewById(R.id.ButtonSin);
        buttonCos              = (Button) findViewById(R.id.ButtonCos);
        buttonTan              = (Button) findViewById(R.id.ButtonTan);
        buttonExp              = (Button) findViewById(R.id.ButtonE);
        buttonASin             = (Button) findViewById(R.id.ButtonASin);
        buttonACos             = (Button) findViewById(R.id.ButtonACos);
        buttonATan             = (Button) findViewById(R.id.ButtonATan);
        buttonPI               = (Button) findViewById(R.id.ButtonPI);

        expressionSettings = ExpressionSettings.builder().mathContext(MathContext.DECIMAL128)
                .powerOperatorPrecedenceHigher().build();

        initPressHandlers();
    }
    public void initPressHandlers() {

        View.OnClickListener onButtonClicked = new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                Button pressedButton = (Button) view;
                String textViewCurrText = (String) textView.getText();
                switch (pressedButton.getId()) {
                    case R.id.ButtonOne:
                        onNonZeroButtonClicked("1");
                        break;
                    case R.id.ButtonTwo:
                        onNonZeroButtonClicked("2");
                        break;
                    case R.id.ButtonThree:
                        onNonZeroButtonClicked("3");
                        break;
                    case R.id.ButtonFour:
                        onNonZeroButtonClicked("4");
                        break;
                    case R.id.ButtonFive:
                        onNonZeroButtonClicked("5");
                        break;
                    case R.id.ButtonSix:
                        onNonZeroButtonClicked("6");
                        break;
                    case R.id.ButtonSeven:
                        onNonZeroButtonClicked("7");
                        break;
                    case R.id.ButtonEight:
                        onNonZeroButtonClicked("8");
                        break;
                    case R.id.ButtonNine:
                        onNonZeroButtonClicked("9");
                        break;
                    case R.id.ButtonZero:
                        onZeroButtonClicked();
                        break;
                    case R.id.ButtonClear:
                        onClearButtonClicked();
                        break;
                    case R.id.ButtonPlusMinus:
                        onPlusMinusButtonClicked();
                        break;
                    case R.id.ButtonPercent:
                        onPercentButtonClicked();
                        break;
                    case R.id.ButtonMul:
                        onBinaryOperationClicked("*");
                        break;
                    case R.id.ButtonDiv:
                        onBinaryOperationClicked("/");
                        break;
                    case R.id.ButtonPlus:
                        onBinaryOperationClicked("+");
                        break;
                    case R.id.ButtonMinus:
                        onBinaryOperationClicked("-");
                        break;
                    case R.id.ButtonDot:
                        onDotButtonClicked();
                        break;
                    case R.id.ButtonLeftParenthesis:
                        onLeftParenthesisButtonClicked();
                        break;
                    case R.id.ButtonRightParenthesis:
                        onRightParenthesisButtonClicked();
                        break;
                    case R.id.ButtonTenInPowerX:
                        onSomethingInPowerXButtonClicked("10");
                        break;
                    case R.id.ButtonLog10:
                        onUnaryOperationClicked("LOG10");
                        break;
                    case R.id.ButtonPowTwo:
                        onPowInSomeExtentButtonClicked("2");
                        break;
                    case R.id.ButtonPowThree:
                        onPowInSomeExtentButtonClicked("3");
                        break;
                    case R.id.ButtonPowY:
                        onBinaryOperationClicked("^");
                        break;
                    case R.id.ButtonPowExpX:
                        onSomethingInPowerXButtonClicked("e");
                        break;
                    case R.id.ButtonSecondRoot:
                        onUnaryOperationClicked("SQRT");
                        break;
                    case R.id.ButtonThirdRoot:
                        onPowInSomeExtentButtonClicked("(1/3)");
                        break;
                    case R.id.ButtonFact:
                        onUnaryOperationClicked("FACT");
                        break;
                    case R.id.ButtonLn:
                        onUnaryOperationClicked("LOG");
                        break;
                    case R.id.ButtonSin:
                        onUnaryOperationClicked("SIN");
                        break;
                    case R.id.ButtonCos:
                        onUnaryOperationClicked("COS");
                        break;
                    case R.id.ButtonTan:
                        onUnaryOperationClicked("TAN");
                        break;
                    case R.id.ButtonE:
                        onSomeConstVariableClicked("e");
                        break;
                    case R.id.ButtonASin:
                        onUnaryOperationClicked("ASIN");
                        break;
                    case R.id.ButtonACos:
                        onUnaryOperationClicked("ACOS");
                        break;
                    case R.id.ButtonATan:
                        onUnaryOperationClicked("ATAN");
                        break;
                    case R.id.ButtonPI:
                        onSomeConstVariableClicked("PI");
                        break;
                    default:
                        // TODO:
                        break;
                }
            }
        };

        View.OnClickListener onEqualButtonClicked = new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                onEqualButtonClicked();
            }
        };

        buttonEqual.setOnClickListener(onEqualButtonClicked);

        buttonOne.setOnClickListener(onButtonClicked);
        buttonTwo.setOnClickListener(onButtonClicked);
        buttonThree.setOnClickListener(onButtonClicked);
        buttonFour.setOnClickListener(onButtonClicked);
        buttonFive.setOnClickListener(onButtonClicked);
        buttonSix.setOnClickListener(onButtonClicked);
        buttonSeven.setOnClickListener(onButtonClicked);
        buttonEight.setOnClickListener(onButtonClicked);
        buttonNine.setOnClickListener(onButtonClicked);
        buttonZero.setOnClickListener(onButtonClicked);
        buttonPlus.setOnClickListener(onButtonClicked);
        buttonMinus.setOnClickListener(onButtonClicked);
        buttonMultiply.setOnClickListener(onButtonClicked);
        buttonDivide.setOnClickListener(onButtonClicked);
        buttonPercent.setOnClickListener(onButtonClicked);
        buttonPlusMinus.setOnClickListener(onButtonClicked);
        buttonClear.setOnClickListener(onButtonClicked);
        buttonDot.setOnClickListener(onButtonClicked);

        if(getScreenOrientation().equals("horizontal")) {
            buttonLeftParenthesis.setOnClickListener(onButtonClicked);
            buttonRightParenthesis.setOnClickListener(onButtonClicked);
            buttonTenInPowerX.setOnClickListener(onButtonClicked);
            buttonLogTen.setOnClickListener(onButtonClicked);
            buttonPowerTwo.setOnClickListener(onButtonClicked);
            buttonPowerThree.setOnClickListener(onButtonClicked);
            buttonPowerY.setOnClickListener(onButtonClicked);
            buttonPowExpX.setOnClickListener(onButtonClicked);
            buttonSecondRoot.setOnClickListener(onButtonClicked);
            buttonThirdRoot.setOnClickListener(onButtonClicked);
            buttonFact.setOnClickListener(onButtonClicked);
            buttonLn.setOnClickListener(onButtonClicked);
            buttonSin.setOnClickListener(onButtonClicked);
            buttonCos.setOnClickListener(onButtonClicked);
            buttonTan.setOnClickListener(onButtonClicked);
            buttonExp.setOnClickListener(onButtonClicked);
            buttonASin.setOnClickListener(onButtonClicked);
            buttonACos.setOnClickListener(onButtonClicked);
            buttonATan.setOnClickListener(onButtonClicked);
            buttonPI.setOnClickListener(onButtonClicked);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("fullExpression", fullExpression);
        outState.putString("lastTextViewValue", textView.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        fullExpression = savedInstanceState.getString("fullExpression");
        textView.setText(savedInstanceState.getString("lastTextViewValue"));
    }

    public void onNonZeroButtonClicked(String number){
        if (textView.getText().equals("0") || isLastOperationBinary()) {
            textView.setText(number);
        } else {
            textView.setText(textView.getText() + number);
        }
        fullExpression += number;
    }

    public void onZeroButtonClicked(){
        if (textView.getText().equals("0")) {
            textView.setText("0");
        } else if(isLastOperationBinary()){
            textView.setText("0");
            fullExpression += "0";
        } else {
            textView.setText(textView.getText() + "0");
            fullExpression += "0";
        }
    }

    public void onClearButtonClicked(){
        if (!textView.getText().equals("0")) {
            textView.setText("0");
            fullExpression = "";
        }
    }

    public void onPlusMinusButtonClicked(){
        String textViewCurrText = (String) textView.getText();
        if (!textViewCurrText.equals("0") && !isLastSymbolOfExpressionIsBinaryOperation()) {
            char firsSymbolOnTextView = 0;
            firsSymbolOnTextView = textViewCurrText.charAt(0);
            if(firsSymbolOnTextView == '-'){
                textView.setText(textViewCurrText.substring(1));
                int lastIndexOfText = fullExpression.lastIndexOf(textViewCurrText);
                fullExpression = fullExpression.substring(0, lastIndexOfText);
                fullExpression += textViewCurrText.substring(1);
            }else{
                textView.setText("-" + textViewCurrText);
                int lastIndexOfText = fullExpression.lastIndexOf(textViewCurrText);
                fullExpression = fullExpression.substring(0, lastIndexOfText);
                fullExpression += "-" + textViewCurrText;
            }
        }
    }

    public void onPercentButtonClicked(){
        String textViewCurrText = (String) textView.getText();
        textView.setText(String.valueOf(new Expression("(" + textViewCurrText + ")/100", expressionSettings).eval().doubleValue()));
        fullExpression = textView.getText().toString();
    }

    public  void onDotButtonClicked(){
        String textViewCurrText = (String) textView.getText();
        if (!textViewCurrText.contains(".")) {
            textView.setText(textViewCurrText + ".");
            fullExpression += ".";
        }
    }

    public  void onEqualButtonClicked(){
        textView.setText(String.valueOf(new Expression(fullExpression, expressionSettings).eval().doubleValue()));
        fullExpression = textView.getText().toString();
        lastOperation = "";
    }

    public void onBinaryOperationClicked(String operation){
        if(lastOperation.equals(operation)){
            return;
        }else if(!lastOperation.equals(operation) && !lastOperation.equals("")){
            fullExpression = fullExpression.substring(0, fullExpression.length() - 1) + operation;
            lastOperation = operation;
        } else {
            fullExpression += /*textViewCurrText +*/ operation;
            lastOperation = operation;
        }
    }

    public void onUnaryOperationClicked(String operation){
        String textViewCurrText = (String) textView.getText();
        fullExpression = fullExpression.substring(0, fullExpression.lastIndexOf(textViewCurrText));
        textView.setText(String.valueOf(new Expression(operation + "(" + textViewCurrText + ")", expressionSettings).eval().doubleValue()));
        fullExpression += textView.getText().toString();
    }

    public  void onLeftParenthesisButtonClicked(){
        fullExpression += "(";
    }

    public  void onRightParenthesisButtonClicked(){
        fullExpression += ")";
    }

    public  void onSomethingInPowerXButtonClicked(String something){
        String textViewCurrText = (String) textView.getText();
        fullExpression = fullExpression.substring(0, fullExpression.lastIndexOf(textViewCurrText));
        textView.setText(String.valueOf(new Expression(something + "^" + textViewCurrText, expressionSettings).eval().doubleValue()));
        fullExpression += textView.getText().toString();
    }

    public void onPowInSomeExtentButtonClicked(String extent){
        String textViewCurrText = (String) textView.getText();
        fullExpression = fullExpression.substring(0, fullExpression.lastIndexOf(textViewCurrText));
        textView.setText(String.valueOf(new Expression(textViewCurrText + "^" + extent, expressionSettings).eval().doubleValue()));
        fullExpression += textView.getText().toString();
    }

    public void onSomeConstVariableClicked(String variable){
        textView.setText(String.valueOf(new Expression(variable, expressionSettings).eval().doubleValue()));
        fullExpression += textView.getText().toString();
    }

    protected boolean isLastOperationBinary(){
        if (lastOperation.equals("+") || lastOperation.equals("-") || lastOperation.equals("*") || lastOperation.equals("/") || lastOperation.equals("^")) {
            lastOperation = "";
            return true;
        }
        lastOperation = "";
        return false;
    }

    protected boolean isLastSymbolOfExpressionIsBinaryOperation(){
        String lastSymbol = fullExpression.substring(fullExpression.length()-1, fullExpression.length());
        if(lastSymbol.equals("+") || lastSymbol.equals("-") || lastSymbol.equals("*") || lastSymbol.equals("/") || lastSymbol.equals("^")){
            return true;
        }
        return  false;
    }

    @NonNull
    private String getScreenOrientation(){
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            return "vertical";
        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            return "horizontal";
        else
            return "";
    }
}